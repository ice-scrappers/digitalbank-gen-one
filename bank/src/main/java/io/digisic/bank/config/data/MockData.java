package io.digisic.bank.config.data;

import java.math.BigDecimal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;



@Component
public class MockData extends SampleData {

	private static final Logger LOG = LoggerFactory.getLogger(MockData.class);
	
	
    
    public void dataSet0()   {
		
		LOG.info("***** Checking Mock Data *******");
		
		BigDecimal num1 = new BigDecimal("10.00");
        BigDecimal num2 = new BigDecimal("20.00");
        BigDecimal num3 = new BigDecimal("30.00");
        
        LOG.info(String.format("%s %s %s",num1,num2,num3));
        LOG.info(String.format("%s %s %s",num1,num2,num3));
        LOG.info(String.format("%s %s %s",num1,num2,num3));
		
		LOG.info("*********************************");

        BigDecimal num10 = new BigDecimal("10.00");
        BigDecimal num20 = new BigDecimal("20.00");
        BigDecimal num30 = new BigDecimal("30.00");
        
        LOG.info(String.format("%s %s %s",num1,num2,num3));
        LOG.info(String.format("%s %s %s",num1,num2,num3));
        LOG.info(String.format("%s %s %s",num1,num2,num3));
	}


     public void dataSet2(String...args)   throws Exception{
		
		LOG.info("***** Checking Mock Data *******");
		
		BigDecimal num1 = new BigDecimal("10.00");
        BigDecimal num2 = new BigDecimal("20.00");
        BigDecimal num3 = new BigDecimal("30.00");
        
        LOG.info(String.format("%s %s %s",num1,num2,num3));
        LOG.info(String.format("%s %s %s",num1,num2,num3));
        LOG.info(String.format("%s %s %s",num1,num2,num3));
		
		LOG.info("*********************************");
	}


       public void dataSet3(String...args)   throws Exception{
		
		LOG.info("***** Checking Mock Data *******");
		
		BigDecimal num1 = new BigDecimal("10.00");
        BigDecimal num2 = new BigDecimal("20.00");
        BigDecimal num3 = new BigDecimal("30.00");
        
        LOG.info(String.format("%s %s %s",num1,num2,num3));
        LOG.info(String.format("%s %s %s",num1,num2,num3));
        LOG.info(String.format("%s %s %s",num1,num2,num3));
		
		LOG.info("*********************************");
	}

	
}
